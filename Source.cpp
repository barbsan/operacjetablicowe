#include <iostream>

using namespace std;

void displayArray(int* arr, int length) {
	for (int i = 0; i < length; i++) {
		cout << *(arr+i) << " ";
	}
	cout << endl;
}

int* map(int * arr, int length, int(*fcn)(int)) {
	int* result = new int[length];
	for (int i = 0; i < length; i++) {
		*(result + i) = (*fcn)(*(arr + i));
	}
	return result;
}


int* filter(int*arr, int length, bool(*fcn)(int), int& count) {
	int* result = new int[length];
	count = 0;
	for (int i = 0; i < length; i++) {
		if ((*fcn)(*(arr + i))) {
			*(result + count) = *(arr + i);
			count++;
		}
	}
	return result;
}

int mnoz2(int a) {
	return a * 2;
}

bool isEven(int a) {
	return (a % 2) == 0;
}

int main() {
	int *result;
	int arr[6] = { 1, 2, 3, 4, 5, 6 };
	int length = sizeof(arr) / sizeof(arr[0]);
	int count = 0;

	cout << "Map: " << endl;
	cout << "Przed: ";
	displayArray(arr, length);
	
	cout << "Po:    ";
	displayArray(map(arr, length, mnoz2), length);

	cout << endl << endl;

	cout << "Filter: " << endl;
	cout << "Przed: ";
	displayArray(arr, length);
	cout << "Po:    ";
	result = filter(arr, length, isEven, count);
	displayArray(result, count);

	system("pause");
	return 0;
}